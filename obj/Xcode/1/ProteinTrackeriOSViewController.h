// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ProteinTrackeriOSViewController : UIViewController {
	UIButton *_addAmountButton;
	UIButton *_addUserButton;
	UITextField *_amountText;
	UILabel *_goalLabel;
	UITextField *_goalText;
	UITextField *_nameText;
	UIPickerView *_selectUserPicker;
	UILabel *_totalLabel;
}

@property (nonatomic, retain) IBOutlet UIButton *addAmountButton;

@property (nonatomic, retain) IBOutlet UIButton *addUserButton;

@property (nonatomic, retain) IBOutlet UITextField *amountText;

@property (nonatomic, retain) IBOutlet UILabel *goalLabel;

@property (nonatomic, retain) IBOutlet UITextField *goalText;

@property (nonatomic, retain) IBOutlet UITextField *nameText;

@property (nonatomic, retain) IBOutlet UIPickerView *selectUserPicker;

@property (nonatomic, retain) IBOutlet UILabel *totalLabel;

@end
